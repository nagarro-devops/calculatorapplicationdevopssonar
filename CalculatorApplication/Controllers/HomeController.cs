﻿using CalculatorApplication.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CalculatorApplication.Controllers
{
    public class HomeController : Controller
    {
       
     

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Calculate(string expression)
        {
            try
            {
                Console.WriteLine(expression);
                var result = Evaluate(expression);
                return Json(new { result = result });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        private int Evaluate(string expression)
        {
            string[] operators = { "+", "-", "*", "/" };
            foreach (var op in operators)
            {
                if (expression.Contains(op))
                {
                    var operands = expression.Split(new string[] { op }, StringSplitOptions.RemoveEmptyEntries);
                    var a = int.Parse(operands[0]);
                    var b = int.Parse(operands[1]);
                    switch (op)
                    {
                        case "+":
                            return Add(a, b);
                        case "-":
                            return Subtract(a, b);
                        case "*":
                            return Multiply(a, b);
                        case "/":
                            return Divide(a, b);
                    }
                }
            }
            throw new Exception("Invalid expression");
        }

        public int Add(int a, int b)
        {
            return a + b;
        }

        public int Subtract(int a, int b)
        {
            return a - b;
        }

        public int Multiply(int a, int b)
        {
            return a * b;
        }

        public int Divide(int a, int b)
        {
            if (b == 0)
                throw new DivideByZeroException();

            return a / b;
        }

    }
}